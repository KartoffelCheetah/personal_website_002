import { h } from 'vue';
import { PW002RenderHtmlPage } from '../util/PW002RenderHtmlPage.js';
import Page, { pageTitle } from './about.vue';
PW002RenderHtmlPage({ title: pageTitle, content: h(Page) });
