import { createSSRApp, h, defineAsyncComponent } from 'vue';
import { renderToString } from 'vue/server-renderer';
import kcIco from '../images/cat007_005_badge_wo_bg.ico';
import kcIco16 from '../images/cat007_005_badge_wo_bg_16.png';
import kcIco32 from '../images/cat007_005_badge_wo_bg_32.png';
const baseLayout = defineAsyncComponent(() => import('../layouts/baseLayout.vue'));

export async function PW002RenderHtmlPage ({
	title,
	content,
}) {
	let isInProd;
	if (['true', 'false'].includes(process.env.IS_PRODUCTION)) {
		isInProd = process.env.IS_PRODUCTION === 'true';
	} else {
		throw new TypeError('process.env.IS_PRODUCTION is not a bool');
	}
	const vueHtml = await (
		renderToString(
			createSSRApp(
				h(
					baseLayout,
					{},
					{
						default: () => content,
					},
				),
			)
		).catch(err => {
			throw err;
		})
	);
	let s;
	try {
		s = 
`<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
		<meta name="description" content="My website.">
		<meta name="author" content="KartoffelCheetah">
		<meta name="keywords" content="kartoffel cheetah front-end developer art illustration">
		<title>${title}</title>
		<link rel="icon" type="image/x-icon" href=${JSON.stringify(kcIco)}>
		<link rel="icon" type="image/png" sizes="32x32" href=${JSON.stringify(kcIco32)}>
		<link rel="icon" type="image/png" sizes="16x16" href=${JSON.stringify(kcIco16)}>
		<style>
			body {
				background: #372837;
				color: #f7e0b4;
			}
		</style>
		<link href="${process.env.PW002_CSS_GLOBAL_BUNDLE.replace(/^dist\//, './')}" rel="stylesheet">
		<link href="${process.env.PW002_CSS_BUNDLE.replace(/^dist\//, './')}" rel="stylesheet">
	</head>
	<body>
		${vueHtml}
		<script>
			window.PW002_IS_HOT_RELOAD_ENABLED = ${!isInProd};
			if (window.PW002_IS_HOT_RELOAD_ENABLED) {
				// https://esbuild.github.io/api/#live-reload
				new EventSource('/esbuild').addEventListener('change', e => {
					location.reload();
				});
			}
		</script>
	</body>
</html>`
		;
	} catch (err) {
		console.log('<h1>500</h1>');
		throw err;
	}
	if (s) {
		console.log(s);
	}
}
