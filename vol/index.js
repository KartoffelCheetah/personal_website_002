#! /usr/local/bin/node
/*
 * ./node_modules/.bin/esbuild app.jsx // echoes app.jsx
 * ./node_modules/.bin/esbuild app.jsx --bundle // echoes app.jsx and all the imported files
 * ./node_modules/.bin/esbuild app.jsx --bundle --outfile=out.js // same but echoes it into a file
 * or you can use the JavaScript api as follows:...
*/
import http from 'node:http'
import fs from 'node:fs';
import path from 'node:path';
import child_process from 'node:child_process';
import * as esbuild from 'esbuild';
import kcEsbuildPluginVueSfc from 'kc-esbuild-plugin-vue-sfc';
import kcEsbuildPluginStylus from 'kc-esbuild-plugin-stylus';

const IS_PROD_BUILD = process.env.NODE_ENV !== 'development';
const STYLESHEET_PREFIX = `kcCwd = ${JSON.stringify(process.cwd())}
@require ${JSON.stringify(path.resolve(process.cwd(), './src/style/utils.styl'))}
`;

(async () => {
	if (fs.existsSync('dist')) {
		console.info('Cleans up old dist folder.');
		fs.rmSync('dist', { recursive: true, });
	}
	const SRC_PAGES = './src/pages/';
	const srcPages = ((await fs.promises.readdir(SRC_PAGES))
		.filter(f=>f.endsWith('.js'))
		.map((srcPageJs) => {
			const pageName = path.parse(srcPageJs).name;
			const result = {
				srcPageInput: `${SRC_PAGES}${pageName}.js`,
				srcPageEndpoints: [
					`/${pageName}.html`,
					`/${pageName}`,
				],
				esbuildOutput: `./dist/pages/${pageName}.js`,
				distOutput: `./dist/${pageName}.html`,
			};
			if (pageName === 'index') {
				result.srcPageEndpoints.push('/');
			}
			return result;
		})
	);

	console.info(`Starts esbuild in ${IS_PROD_BUILD ? 'PROD': 'DEV'}...`);
	const ctx = await esbuild[IS_PROD_BUILD ? 'build' : 'context']({
		metafile: true,
		entryPoints: [
			'src/global.js',
			...srcPages.map(s => s.srcPageInput),
		],
		entryNames: IS_PROD_BUILD ? '[dir]/[name]-[hash]' : undefined,
		splitting: true,
		format: 'esm',
		outdir: 'dist',
		bundle: true,
		sourcemap: true,
		minify: false,
		loader: {
			'.jpg': 'file',
			'.png': 'file',
			'.webp': 'file',
			'.ico': 'file',
			'.ttf': 'file',
			'.woff2': 'file',
		},
		define: {
			__VUE_PROD_DEVTOOLS__: 'true',
			__VUE_OPTIONS_API__: 'false',
		},
		platform: 'node',
		target: [
			'node20',
		],
		plugins: [
			kcEsbuildPluginVueSfc({
				preprocessorPrefixes: {
					'stylus': STYLESHEET_PREFIX,
				},
			}),
			kcEsbuildPluginStylus({
				verbose: true,
				stylusUseApiCallback (stylusApi) {
					stylusApi.str = STYLESHEET_PREFIX + stylusApi.str;
				},
			}),
		],
	});
	if (IS_PROD_BUILD) {
		runSsr(srcPages, ctx);
	} else {
		const PORT_PROXY = 4200;
		await ctx.watch();
		const esbuildServe = await ctx.serve({
			servedir: './dist',
		});
		console.info('Esbuild serve', esbuildServe);
		(http
			.createServer(
				devServer.bind(
					null,
					{
						contentServer: esbuildServe,
						srcPages,
					},
				)
			)
			.listen(PORT_PROXY)
		);
		console.info('Proxy serve', PORT_PROXY);
	}
})();

function devServer ({ contentServer, srcPages }, req, res) {
	console.info(`${req.method} ${req.url}`);
	const srcPage = srcPages.find(s => s.srcPageEndpoints.includes(req.url));
	if (srcPage) {
		const proc = child_process.spawn(
			'node',
			[srcPage.esbuildOutput],
			{
				env: {
					...process.env,
					IS_PRODUCTION: IS_PROD_BUILD,
					PW002_ENDPOINT: srcPage.srcPageEndpoints[0],
					PW002_CSS_BUNDLE: srcPage.srcPageInput.replace(/.js$/, '.css').replace('/src/', '/'),
					PW002_CSS_GLOBAL_BUNDLE: './global.css',
				},
			},
		);
		proc.stdout.pipe(fs.createWriteStream(srcPage.distOutput));
		proc.stderr.pipe(process.stderr);
		proc.on('error', err => console.error(err));
		proc.on('close', async (exitCode) => {
			const resStatus = exitCode ? 500 : 200;
			console.info(resStatus);
			res.writeHead(
				resStatus,
				{ 'Content-Type': 'text/html' },
			);
			fs.createReadStream(srcPage.distOutput).pipe(res);
		});
	} else {
		const proxyReq = http.request(
			{
				hostname: contentServer.host,
				port: contentServer.port,
				path: req.url,
				method: req.method,
				headers: req.headers,
			},
			(proxyRes) => {
				if (proxyRes.statusCode === 404) {
					res.writeHead(
						404,
						{ 'Content-Type': 'text/html' },
					);
					res.end('<h1>404</h1>');
				} else {
					res.writeHead(proxyRes.statusCode, proxyRes.headers);
					proxyRes.pipe(res, { end: true });
				}
			},
		);
		req.pipe(proxyReq, { end: true });
	}
}
function extractEntryPointBundles (ctx, srcPages) {
	const metaOutputs = ctx.metafile.outputs;
	const entryPoints = srcPages.map(p => p.srcPageInput.replace(/^\.\//, ''));
	const pageEntryPointBundles = [];
	let globalEntryPointBundle = '';
	for (const outputName in metaOutputs) {
		const metaOutput = metaOutputs[outputName];
		if (metaOutput.entryPoint === 'src/global.js') {
			globalEntryPointBundle = metaOutput;
		}
		const idx = entryPoints.indexOf(metaOutput.entryPoint);
		if (idx !== -1) {
			pageEntryPointBundles.push({
				metaOutputs,
				outputName,
			});
		}
	}
	return {
		globalEntryPointBundle,
		pageEntryPointBundles,
	};
}
async function runSsr (srcPages, ctx) {
	const { pageEntryPointBundles, globalEntryPointBundle } = extractEntryPointBundles(ctx, srcPages);
	await Promise.all(srcPages
		.map(({ srcPageEndpoints, distOutput }, idx) => {
			const { outputName, metaOutputs } = pageEntryPointBundles[idx];
			console.info(`Building ${outputName} -> ${distOutput}...`);
			if (!fs.existsSync(outputName)) {
				console.error('No file: ', distOutput);
				process.exit(1)
			}
			const proc = child_process.spawn(
				'node',
				[outputName],
				{
					env: {
						...process.env,
						IS_PRODUCTION: IS_PROD_BUILD,
						PW002_ENDPOINT: srcPageEndpoints[0],
						PW002_CSS_BUNDLE: metaOutputs[outputName].cssBundle,
						PW002_CSS_GLOBAL_BUNDLE: globalEntryPointBundle.cssBundle,
					},
				},
			)
			proc.stdout.pipe(fs.createWriteStream(distOutput));
			proc.stderr.pipe(process.stderr);
			return new Promise((resolve, reject) => {
				proc.on('close', () => {
					console.info(`Built ${distOutput}.`);
					resolve();
				});
				proc.on('error', (err) => {
					reject();
				});
			});
		})
	);
	console.info('Build finished.');
}
