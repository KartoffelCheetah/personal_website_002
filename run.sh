#!/usr/bin/sh

echo "
	0. Make sure to set the .env file correctly.
	1. You can run this program by executing 'node .' inside the volume.
"

IMG_TAG=localhost/pw002

podman image build\
	-t "$IMG_TAG"\
	-f ./Containerfile\
	.

buildStatusCode="$?"

if [ "$buildStatusCode" = '0' ];then
	podman container run\
		-it\
		--rm\
		--workdir /home/node\
		-v ./vol:/home/node\
		-p 4200:4200\
		--env-file ./.env\
		"$IMG_TAG"\
		sh
else
	exit "$buildStatusCode"
fi
